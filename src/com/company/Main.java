package com.company;

public class Main {

    public static void main(String[] args) {
        Human h = new Human("Zots", "Max", "PETROVICH");
        System.out.println(h.getFullName());
        h.setFullName("Zots", "Max", "Vitalievich");
        System.out.println(h.getFullName());
        h.setFullName("Zots", "Maxim", null);
        System.out.println(h.getFullName());
        h.setFullName("Zots", "Max", "Vitalievich");
        System.out.println(h.getShortName());
        h.setFullName("Zots", "Maxim", null);
        System.out.println(h.getShortName());
    }
}
