package com.company;

public class Human {
    private String name;
    private String surname;
    private String patronymic;


    public Human(String surname, String name) {
        this.name = name;
        this.surname = surname;
    }

    public Human(String surname, String name, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public String getFullName() {
        if (patronymic == null) {
            return surname + " " + name;
        } else {
            return surname + " " + name + " " + patronymic;

        }
    }

    public String getShortName () {
        if (patronymic == null) {
            return surname + " " + name.charAt(0);
        } else {
            return surname + " " + name.charAt(0) + " " + patronymic.charAt(0);
        }
    }

    public void setFullName(String surname, String name, String patronymic) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

}
